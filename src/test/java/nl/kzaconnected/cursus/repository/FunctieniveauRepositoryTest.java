package nl.kzaconnected.cursus.repository;

import nl.kzaconnected.cursus.model.Dao.Functieniveau;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class FunctieniveauRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private FunctieniveauRepository FunctieniveauRepository;

    @Test
    public void findByFunctieniveauShouldReturnFunctieniveauWithId() {
        Functieniveau functieniveau = Functieniveau.builder().functieniveau("Functieniveau1").build();
        entityManager.persist(functieniveau);
        entityManager.flush();
        Functieniveau found = FunctieniveauRepository.findByFunctieniveau(functieniveau.getFunctieniveau());
        assertThat(found.getId()).isNotNull();
        assertThat(found.getFunctieniveau()).isEqualTo(functieniveau.getFunctieniveau());
    }
}